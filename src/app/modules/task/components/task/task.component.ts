import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Task } from '../../../../core/+store/tasks/entity/task.model';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent {
  @Input() task: Task;
  @Output() completed = new EventEmitter<Task>();

  toggleComplete(task: Task): void {
    this.completed.emit(task);
  }

}
