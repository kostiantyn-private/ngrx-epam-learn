import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { AppState } from '../../../../core/+store';
import { Observable } from 'rxjs';
import { TaskState } from '../../../../core/+store/tasks';
import { Task } from '../../../../core/+store/tasks/entity/task.model';
import * as TasksActions from '../../../../core/+store/tasks/task.action';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {
  tasksState$: Observable<TaskState>;

  constructor(
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.tasksState$ = this.store.pipe(select('tasks'));
  }

  onCompleted(task: Task): void {
    this.store.dispatch(new TasksActions.DoneTask(task));
  }

}
