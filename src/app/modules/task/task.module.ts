import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { tasksReducer } from '../../core/+store/tasks';
import { TaskListComponent } from './components/task-list/task-list.component';
import { TaskComponent } from './components/task/task.component';
import { EffectsModule } from '@ngrx/effects';
import { TasksEffects } from '../../core/+store/tasks/task.effects';

@NgModule({
  declarations: [
    TaskListComponent,
    TaskComponent
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature('tasks', tasksReducer),
    EffectsModule.forFeature([TasksEffects])
  ],
  exports: [
    TaskListComponent
  ]
})
export class TaskModule { }
