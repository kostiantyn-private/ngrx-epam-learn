import { TaskState } from './tasks';

export interface AppState {
  tasks: TaskState;
}
