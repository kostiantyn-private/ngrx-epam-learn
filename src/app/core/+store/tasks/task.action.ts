import { Action } from '@ngrx/store';
import { Task } from './entity/task.model';

export enum TaskActionTypes {
  GET_TASKS = '[Tasks] GET_TASKS',
  DONE_TASK = '[Tasks] DONE_TASK'
}

export class GetTasks implements Action {
  readonly type = TaskActionTypes.GET_TASKS;
  public payload;
}

export class DoneTask implements Action {
  readonly type = TaskActionTypes.DONE_TASK;
  constructor(public payload: Task) {}
}

export type TasksActions =
  | GetTasks
  | DoneTask;
