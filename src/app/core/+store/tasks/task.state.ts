import { Task } from './entity/task.model';

export interface TaskState {
  data: ReadonlyArray<Task>;
}

export const initialTaskState: TaskState = {
  data: [
    new Task(1, 'learn english', true),
    new Task(2, 'learn ngrx', false)
  ]
};
