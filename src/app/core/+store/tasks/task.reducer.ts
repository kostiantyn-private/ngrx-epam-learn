import { initialTaskState, TaskState } from './task.state';
import { TaskActionTypes, TasksActions } from './task.action';
import { Task } from './entity/task.model';

export function tasksReducer(
  state = initialTaskState,
  action: TasksActions
): TaskState {
  console.log(`Reducer: Action came in! ${action.type}`);

  switch (action.type) {
    case TaskActionTypes.GET_TASKS: {
      console.log('GET_TASK action being handled');
      return { ...state };
    }
    case TaskActionTypes.DONE_TASK: {
      console.log('DONE_TASK action being handled');

      const id = (<Task>action.payload).id;
      const data = state.data.map(task => {
        if (task.id === id) {
          return { ...action.payload, isDone: !task.isDone };
        }
        return task;
      });

      return { ...state, data };
    }
    default: {
      console.log('UNKNOWN_TASK action being handled');
      return state;
    }
  }
}
